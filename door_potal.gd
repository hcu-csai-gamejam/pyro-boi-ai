extends Area2D

signal unlocksignal
signal sent

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func unlockDoor():
	emit_signal("unlocksignal")


func _on_body_entered(body):
	emit_signal("sent",body.name)
#	if(body.name=="water player"):
#		get_tree().change_scene_to_file("res://menu.tscn")
