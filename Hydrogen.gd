extends Area2D

signal  sent
func _on_body_entered(body):
	if(body.name=="water player"):
		queue_free()
		$AudioStreamPlayer.play()
		emit_signal("sent")
