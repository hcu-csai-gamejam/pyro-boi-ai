extends Node2D

@onready var anim :AnimationPlayer = $AnimatableBody2D/AnimationPlayer

func start_moving():
	print('hello from platform')
	anim.play("moving")

func reset():
	print('hello from platform')
	if position.y != 0:
		anim.play_backwards("moving")
