extends StaticBody2D

class_name PressurePlate

@onready var animated_sprite : AnimatedSprite2D = $AnimatedSprite2D
@onready var unpressed_collision : CollisionPolygon2D = $UnpressedCollision
@onready var pressed_collision : CollisionPolygon2D = $PressedCollision

var is_button_pressed = true
#func _ready():
	#get_shape_owners ( )
	
func press_button() -> void:
	is_button_pressed = true
	#unpressed_collision.set_deferred("disabled",true)
	#pressed_collision.set_deferred("disabled",false)
	animated_sprite.play("pressed")

func unpress_button() -> void:
	is_button_pressed = false
	#unpressed_collision.set_deferred("disabled",false)
	#pressed_collision.set_deferred("disabled",true)
	animated_sprite.play("unpressed")

func _on_pressed_detector_body_entered(body):
	print('enter %s'%body)
	control_connected(true)
	if body is player :
		if is_button_pressed == false:
			press_button()


func _on_pressed_detector_body_exited(body):
	control_connected(false)
	if body is player :
		if is_button_pressed == true:
			unpress_button()

func control_connected(i : bool):
	if i == true:
		get_node("movingplatform2").start_moving()
	else:
		get_node("movingplatform2").reset()
