extends  CharacterBody2D

class_name player

@export var health = 100

@export var speed : int

@onready var sprite : Sprite2D = $Sprite2D
@export var controls : Resource = null

@onready var statemachine :CharacterStateMachine = $CharacterStateMachine
@onready var animation_tree : AnimationTree = $AnimationTree

@export var jump_height : float
@export var jump_time_to_peak : float
@export var jump_time_to_descent : float

@onready var jump_velocity : float = ((2.0 * jump_height) / jump_time_to_peak)*-1.0
@onready var jump_gravity : float = ((-2.0 * jump_height) / (jump_time_to_peak*jump_time_to_peak))*-1.0
@onready var fall_gravity : float = ((-2.0 * jump_height) / (jump_time_to_descent*jump_time_to_descent))*-1.0


var player_state = ""
var direction : int

func _ready():
	var layer1 = get_collision_layer_value(1)
	print("{0} {1}".format([name,str(layer1)]))
	animation_tree.active = true
	#print('jello')
func get_gravity() -> float:
	return jump_gravity if velocity.y < 0.0 else fall_gravity

##define main character gravity
func _physics_process(delta):
	velocity.y += get_gravity() * delta
	direction = int(Input.get_axis(controls.move_left,controls.move_right))
	_flip_sprite(direction)
	move_and_slide()

func _flip_sprite(d:float):
	if d < 0:
		sprite.flip_h = true
	if d > 0:
		sprite.flip_h = false
		
