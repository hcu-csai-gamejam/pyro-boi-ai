extends State

class_name GroundState
@export var jump_start : String = "jump_start"
@export var air_state = State
@export var push_pull_state = State

func on_enter():
	character.animation_tree.set("parameters/Transition/transition_request","ground")
	character.animation_tree.set("parameters/onGround_transition/transition_request","walk_state")

func state_process(delta):
	if(!character.is_on_floor()):
		next_state = air_state
		character.animation_tree.set("parameters/Transition/transition_request","air")
		playback.travel("jump_falling")

func state_input(event: InputEvent):
	if(event.is_action_pressed(character.controls.jump)):
		#print("now press jump")
		jump()
	if(event.is_action_pressed(character.controls.push_interaction)):
		next_state = push_pull_state
		
func jump():
	character.velocity.y = character.jump_velocity
	next_state = air_state
	character.animation_tree.set("parameters/Transition/transition_request","air")
	playback.travel("jump_start")

func _physics_process(delta) -> void:
	if character.direction != 0 and character.statemachine.check_if_can_move() :
		character.animation_tree.set("parameters/walk_blend/blend_position",character.direction)
		character.velocity.x = character.direction * character.speed
	elif character.direction == 0 :
		character.animation_tree.set("parameters/walk_blend/blend_position",character.direction)
		character.velocity.x = 0
