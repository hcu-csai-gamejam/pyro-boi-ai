extends State

@export var ground_state = State

func on_enter():
	print("now in the push pull")
	character.animation_tree.set("parameters/onGround_transition/transition_request","push_pull_state")

func state_input(event : InputEvent):
	if(event.is_action_pressed(character.controls.push_interaction)):
		next_state = ground_state

func _physics_process(delta):
	if character.direction < 0 and character.statemachine.check_if_can_move() :
		character.animation_tree.set("parameters/push_pull_blend/blend_position",character.direction)
		character.velocity.x = character.direction * character.speed
	elif character.direction > 0 and character.statemachine.check_if_can_move():
		character.animation_tree.set("parameters/push_pull_blend/blend_position",character.direction)
		character.sprite.flip_h = false
		character.velocity.x = character.direction * character.speed
	elif character.direction == 0 :
		character.animation_tree.set("parameters/push_pull_blend/blend_position",character.direction)
		character.velocity.x = 0
