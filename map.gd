extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var score=0
var fire = false
var water = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	_on_door_potal_unlocksignal()
	if(fire==true && water==true):
		winquit()
	
	
func _on_gasoline_sent():
	score +=1
	print(score)
	$coin.play()
	


func _on_gasoline_2_sent():
	score +=1
	print(score)
	$coin.play()


func _on_gasoline_3_sent():
	score +=1
	print(score)
	$coin.play()


func _on_hydrogen_sent():
	score +=1
	print(score)
	$coin.play()


func _on_hydrogen_3_sent():
	score +=1
	print(score)
	$coin.play()


func _on_hydrogen_2_sent():
	score +=1
	print(score)
	$coin.play()
	


func _on_door_potal_sent(in_name):
	if (score == 6 && in_name == "fire player"):
		fire=true
#		get_tree().change_scene_to_file("res://menu.tscn")
	


func _on_door_potal_unlocksignal():
	if score ==6 :
		$"door potal/AnimatedSprite2D".play("fire unlock")
		$"door potal2/AnimatedSprite2D".play("unlock")


func _on_door_potal_2_sent(in_name):
	if (score == 6 && in_name == "water player"):
		water= true
	#	get_tree().change_scene_to_file("res://menu.tscn")


func _on_door_potal_body_exited(body):
	if(body.name== "fire player"):
		fire= false
	else:
		water= false


func _on_door_potal_2_body_exited(body):
	if(body.name== "water player"):
		water= false
	else:
		fire= false

func winquit():
	$win.play()
	while ($win.playing):
		print("playing")
	get_tree().change_scene_to_file("res://endscene.tscn")
